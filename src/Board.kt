class Board(boardSize: Int, private var gridSize: Int) : Grid<SubGrid>(boardSize, SubGrid(gridSize)) {

    /**
     * In Board the variable `size` represents the number of sub grids
     */

    /**
     * If set to true then wee don't allow the inserting of subgrids. This allows the blocking of the subgrids during
     * gameplay and only allowing the inserting of single values at specific positions.
     */
    var locked: Boolean = false


    override fun getRowString(row: Int): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getColumnString(column: Int): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun insert(value: SubGrid, x: Int, y: Int) {
        if (locked) {
            throw Throwable("Board is locked and does not allow the editing of whole subgrids")
        } else {
            // TODO: Update to use 1D logic
//            grid[x][y] = value
        }
    }

    override fun isValidInput(input: SubGrid, x: Int, y: Int): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isComplete(): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun toString(): String {
        val subGridsRowsString = Array(size * gridSize + (size / 2)) { i ->
            if (i > 0 && i % gridSize == 0) {
                "-".repeat(gridSize * 2 * size + size - 1)
            } else {
                ""
            }
        }

        for (row in 0 until size * gridSize + (size / 2)) {
            if (row > 0 && row % gridSize == 0) {
                continue
            }

            val subGridsRowString = Array(size) { _ -> "" }
            for (subGrid in 0 until size) {
                subGridsRowString[subGrid] = grid[subGrid * row].getRowString(row / gridSize)
            }

            subGridsRowsString[row] = subGridsRowString.joinToString(" | ")
        }

        return subGridsRowsString.joinToString("\n")
    }
}