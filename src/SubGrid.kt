class SubGrid(size: Int) : Grid<Int>(size, 0) {

    override fun getRowString(row: Int): String {
        return grid.slice((row * size)..(row * size + size - 1)).joinToString(" ")
    }

    override fun getColumnString(column: Int): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun insert(value: Int, x: Int, y: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isValidInput(input: Int, x: Int, y: Int): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isComplete(): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun toString(): String {
        val rowArray = Array(size) { _ -> "" }
        for (index in 0 until size step size) {
            rowArray[index / size] = grid.slice(index..(index + size - 1)).joinToString(" ")
        }

        // Not sure why but if this is joined with a \n then a double \n is added instead
        // Must be some magic inside the joinToString function -\0/-
        return rowArray.joinToString("")
    }
}