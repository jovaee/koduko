abstract class Grid<A>(val size: Int, val default: A) {

    /**
     *
     */
    var grid: MutableList<A> = MutableList(size * size) { _ -> default }

    /**
     *
     */
    abstract fun insert(value: A, x: Int, y: Int)

    /**
     *
     */
    abstract fun isValidInput(input: A, x: Int, y: Int): Boolean

    /**
     *
     */
    abstract fun isComplete(): Boolean

    /**
     *
     */
    abstract fun getRowString(row: Int): String

    /**
     *
     */
    abstract fun getColumnString(column: Int): String
}